package jaumebalmes.dam.m06.com.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import jaumebalmes.dam.m06.com.GameClass;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Pocket Dungeon";
		config.useGL30 = true;
		config.height = 1080;
		config.width = 1920;
		new LwjglApplication(new GameClass(), config);
	}
}
