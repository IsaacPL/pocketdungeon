package jaumebalmes.dam.m06.com.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

import jaumebalmes.dam.m06.com.GameClass;
import jaumebalmes.dam.m06.com.GameController;
import jaumebalmes.dam.m06.com.screens.PlayScreen;

public class Enemy extends Sprite {
    private GameController controller;
    private int random,holdValue;

    public enum State {WALKING, STANDING, ATTACKING, DEAD}

    public Character.State currentState, previousState;
    public World world;
    public Body body;
    private TextureRegion characterStandIdleInitialFrame;
    private Animation<TextureRegion> characterWalk, characterStandIdle, characterAttack, characterDeath;
    private float stateTimer;
    private boolean walkingRight, isAttacking, isAlive;

    //aaaaa
    private Sound deathYell;

    private float xPosition, yPosition;
    //

    public Enemy(World world, PlayScreen screen, TextureAtlas dummy, float xPosition, float yPosition) {
        //super(screen.getAtlas().findRegion("goblin"));
        super(dummy.findRegion("goblin_spritesheet_calciumtrice_grande"));
        this.world = world;

        this.xPosition = xPosition;
        this.yPosition = yPosition;

        currentState = Character.State.STANDING;
        previousState = Character.State.STANDING;
        stateTimer = 0;
        walkingRight = true;
        isAttacking = false;
        isAlive = true;

        random = new Random().nextInt(2);
        holdValue = random;

        holdValue = random == 0 ?  1340 : 545;

        deathYell = random == 0 ? Gdx.audio.newSound(Gdx.files.internal("audio/sounds/enemydeath.mp3")) : Gdx.audio.newSound(Gdx.files.internal("audio/sounds/enemydeath2.mp3"));

        //  Animación idle
        Array<TextureRegion> frames = new Array<TextureRegion>();
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 0, 160, 162));
        characterStandIdle = new Animation(0.15f, frames);
        frames.clear();

        //  Animación caminar 1340
        holdValue = random == 0 ?  1340 : 545;
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, holdValue, 160, 160));
        characterWalk = new Animation(0.125f, frames);
        frames.clear();

        // Animación atacar
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, 161 * 3, 160, 160));
        characterAttack = new Animation(0.09f, frames);
        frames.clear();

        // Animación morir 1660
        holdValue = random == 0 ?  1660 : 868;
        for (int i = 0; i < 10; i++)
            frames.add(new TextureRegion(getTexture(), i * 160, holdValue, 170, 160));
        characterDeath = new Animation(0.15f, frames);
        frames.clear();

        //  Frame inicial
        characterStandIdleInitialFrame = new TextureRegion(getTexture(), 0, 0, 160, 160);

        defineCharacter();
        setBounds(0, 0, characterStandIdleInitialFrame.getRegionWidth() / 2 / GameClass.PPM, characterStandIdleInitialFrame.getRegionHeight() / 2 / GameClass.PPM);
        setRegion(characterStandIdleInitialFrame);

        //aaaaaaaaaaaaaaaaaaaaaaaaaaa
        //body.getPosition().x
        //
    }

    public void update(float dt) {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
        setRegion(getFrame(dt));

        movementAnimation();
    }

    public TextureRegion getFrame(Float dt) {
        currentState = getState();

        TextureRegion region;
        switch (currentState) {
            case WALKING:
                region = characterWalk.getKeyFrame(stateTimer, true);

                break;
            case ATTACKING:
                region = characterAttack.getKeyFrame(stateTimer, false);

                break;
            case DEAD:
                region = characterDeath.getKeyFrame(stateTimer, false);

                if (stateTimer >= 0.1 && this.body.getFixtureList().size > 0)
                    this.body.destroyFixture(this.body.getFixtureList().get(0));

                break;
            case STANDING:
            default:
                region = characterStandIdle.getKeyFrame(stateTimer, true);
        }

        //Actualizar la dirección a la que apuntó por última vez el personaje
        if ((body.getLinearVelocity().x < 0 || !walkingRight) && !region.isFlipX()) {
            region.flip(true, false);
            walkingRight = false;
        } else if ((body.getLinearVelocity().x > 0 || walkingRight) && region.isFlipX()) {
            region.flip(true, false);
            walkingRight = true;
        }

        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        previousState = currentState;

        return region;
    }

    private Character.State getState() {
        //prueba debug muerte
        if (!isAlive) {
            this.body.setType(BodyDef.BodyType.StaticBody);
            return Character.State.DEAD;
            //
        } else {
            if (body.getLinearVelocity().x != 0 || body.getLinearVelocity().y != 0)
                return Character.State.WALKING;
            //provisional
            if (isAttacking)
                return Character.State.ATTACKING;
            else
                return Character.State.STANDING;
        }
    }

    public void defineCharacter() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(xPosition / GameClass.PPM, yPosition / GameClass.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape polygonShape = new PolygonShape();

        polygonShape.setAsBox(12 / GameClass.PPM, 16 / GameClass.PPM);
        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);
    }

    public boolean getCurrentlyAttacking() {
        return isAttacking;
    }

    public void triggerAttack(GameController controller) {
        isAttacking = true;
        this.controller = controller;
    }

    public void endAttack() {
        isAttacking = false;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void killCharacter() {
        isAlive = false;
        deathYell.setVolume(deathYell.play(),0.2f);

        //this.body.destroyFixture(this.body.getFixtureList().get(0));
    }

    public void movementAnimation() {
        if (body.getLinearVelocity().x == 0 && walkingRight) {
            body.setLinearVelocity(-0.5f, 0);
        }

        if (body.getLinearVelocity().x == 0 && !walkingRight) {
            body.setLinearVelocity(0.5f, 0);
        }
    }
}
