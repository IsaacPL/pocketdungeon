package jaumebalmes.dam.m06.com.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import jaumebalmes.dam.m06.com.GameClass;
import jaumebalmes.dam.m06.com.GameController;
import jaumebalmes.dam.m06.com.overlay.Hud;
import jaumebalmes.dam.m06.com.sprites.Character;
import jaumebalmes.dam.m06.com.sprites.Enemy;
import jaumebalmes.dam.m06.com.tools.B2WorldCreator;

import java.util.ArrayList;
import java.util.Random;

public class PlayScreen implements Screen {
    private GameClass game;
    private TextureAtlas textureAtlas;

    private OrthographicCamera camera;
    private Viewport gamePort;
    private Hud hud;

    // Variables del Tiled map
    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    //  Variables del Box2d
    private World world;
    private Box2DDebugRenderer b2dr;

    //  Player
    private Character player;

    private GameController controller;

    //Enemy
    private Enemy enemy;
    private ArrayList<Enemy> enemies;


    private float initialTime;
    private int INITIAL_TIME;

    //private TextureRegion darknessOpacity;

    //textura GameOver
    private Texture texture;
    private float opacity;
    private Sprite gameOverSprite, successSprite;
    //

    private int enemyCounter;
    private Music dungeonTheme, gameOverTheme, victoryTheme;

    private float afterScreenLapse;

    private int lastButtonPressed;

    public PlayScreen(GameClass game, int hp, int level) {
        this.game = game;

        textureAtlas = new TextureAtlas("characterAtlas.atlas");
        TextureAtlas goblin = new TextureAtlas("goblin.atlas");
        //darknessOpacity = new TextureRegion(goblin.findRegion("darkness_opacity"));

        //  Instanciar cámara que sigue al personaje a través de la cámara del mundo
        camera = new OrthographicCamera();

        //  ViewPort que mantiene el ratio de pantalla independientemente del ratio
        //  del dispositivo. Añade barras negras donde no renderiza nada.
        gamePort = new FitViewport(GameClass.V_WIDTH / GameClass.PPM, GameClass.V_HEIGHT / GameClass.PPM, camera);

        //  HUD donde se muestra la puntuación/tiempo/información del nivel
        hud = new Hud(GameClass.sb);

        // Cargar el mapa y configurar el renderizador del mapa
        mapLoader = new TmxMapLoader();

        map = mapLoader.load("map/GameMap.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / GameClass.PPM);

        //  Setar inicialmente la cámara centrada correctamente al inicio de
        camera.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

        //world = new World(new Vector2(0, -10), true);
        world = new World(new Vector2(0, 0), true);
        b2dr = new Box2DDebugRenderer();

        new B2WorldCreator(world, map);

        // Setar personaje, controles
        player = new Character(world, this, hp);
        controller = new GameController();
        player.body.getFixtureList().get(0).setUserData(player);

        instanceEnemies(goblin,level);
        enemyCounter = enemies.size();

        //setar por array.size
        initialTime = 240;

        INITIAL_TIME = (int) initialTime;

        texture = new Texture("GameOver.png");
        gameOverSprite = new Sprite(texture);
        texture = new Texture("Success.png");
        successSprite = new Sprite(texture);

        setMusicClips();

        System.out.println(player.body.getFixtureList());
    }

    public TextureAtlas getAtlas() {
        return textureAtlas;
    }

    @Override
    public void show() {

    }

    public void handleInput(float dt) {

        if (!controller.isUpPressed() || !controller.isDownPressed() || !controller.isRightPressed() || !controller.isLeftPressed())
            player.body.setLinearVelocity(new Vector2(0, 0));

        if (controller.isUpPressed()) {
            player.body.setLinearVelocity(new Vector2(0, 0.7f));
            lastButtonPressed = 1;
        }
        if (controller.isRightPressed()) {
            player.body.setLinearVelocity(new Vector2(0.7f, 0));
            lastButtonPressed = 2;
        }
        if (controller.isLeftPressed()) {
            player.body.setLinearVelocity(new Vector2(-0.7f, 0));
            lastButtonPressed = 3;
        }
        if (controller.isDownPressed()) {
            player.body.setLinearVelocity(new Vector2(0, -0.7f));
            lastButtonPressed = 4;
        }

        //Provisional
        if (controller.isAttackPressed() && !player.getCurrentlyAttacking()) {
            controller.getStage().getRoot().setTouchable(Touchable.disabled);
            if (player.isWalkingRight()) {
                player.setCurrentSwordXPosition(player.body.getPosition().x - 5, player.body.getPosition().y - 5);
            } else {
                player.setCurrentSwordXPosition(player.body.getPosition().x, player.body.getPosition().y - 5);
            }
            player.triggerAttack(controller);
        }
    }

    public void update(float dt) {
        world.step(1 / 60f, 6, 2);

        player.update(dt);

        for (Enemy enemy : enemies) {
            enemy.update(dt);
        }

        camera.position.x = player.body.getPosition().x;
        camera.position.y = player.body.getPosition().y;

        camera.update();
        renderer.setView(camera);

        if (player.getGameOverLapse() || !player.isAlive()) {
            hud.updateLabels(0, enemyCounter, initialTime, INITIAL_TIME);
            controller.removeInputLayout();
            hud.stage.dispose();

            if (player.body.getFixtureList().size > 0)
                player.body.destroyFixture(player.body.getFixtureList().get(0));

            handleTouchEvent(dt);
        } else {
            hud.updateLabels(player.getCharacterHP(), enemyCounter, initialTime, INITIAL_TIME);
            initialTime -= dt;

            checkCollisions(dt);
            handleInput(dt);

            if (initialTime <= 0)
                player.killCharacter(dungeonTheme, gameOverTheme);

            if (getCurrentEnemyCounter() == 0) {
                controller.removeInputLayout();
                hud.stage.dispose();
                handleTouchEvent(dt);
            }
        }
    }

    @Override
    public void render(float delta) {
        update(delta);

        //  Pinta la pantalla de negro
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Renderizar el mapa del juego
        renderer.render();

        // Renderizar el Box2DebugLines
        //b2dr.render(world, camera.combined);

        GameClass.sb.setProjectionMatrix(camera.combined);
        GameClass.sb.begin();

        if (player.isAlive()) {

            for (Enemy enemy : enemies) {
                enemy.draw(GameClass.sb);
            }

            player.draw(GameClass.sb);

            if (getCurrentEnemyCounter() == 0) {
                succesTextureUpdate(delta);
                successSprite.draw(GameClass.sb);
            }

        } else {

            player.draw(GameClass.sb);

            for (Enemy enemy : enemies) {
                enemy.draw(GameClass.sb);
            }

            if (getCurrentEnemyCounter() == 0) {
                succesTextureUpdate(delta);
                successSprite.draw(GameClass.sb);
            }
            if (player.getGameOverLapse()) {

                gameOverTextureUpdate(delta);
                gameOverSprite.draw(GameClass.sb);
            }
        }

        GameClass.sb.end();

        //Si el dispositivo es Android aparece el layout, si no desaparece
        if (Gdx.app.getType() == Application.ApplicationType.Android)
            controller.draw();

        //  Setear el SpriteBatch para dibujar lo que ve la cámara
        GameClass.sb.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        b2dr.dispose();
        hud.dispose();
        world.dispose();
        map.dispose();
        renderer.dispose();
        dungeonTheme.dispose();
        victoryTheme.dispose();
        gameOverTheme.dispose();
    }

    public void checkCollisions(final float dt) {
        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                for (Enemy e : enemies) {
                    if ((contact.getFixtureA().getUserData() == player && !contact.getFixtureA().isSensor()) && contact.getFixtureB().getUserData() == e) {
                        player.body.getFixtureList().get(0).setSensor(true);
                        player.substractCharacterHP();
                        if (player.getCharacterHP() > 0) {
                            Gdx.input.vibrate(100);
                            player.setWounded(true);
                            player.setDefaultCharacterColor(new Color(player.getColor()));

                            //Cambia el color del pj a rojo y lo tira hacia atrás
                            moveAccordingLastDirection(dt);
                            //player.setColor(c);
                        } else {
                            //System.out.println("El personaje ha tocado al enemigo");
                            player.killCharacter(dungeonTheme, gameOverTheme);
                            controller.removeInputLayout();
                        }
                    }
                    //provisional colisión espada casi hecho falta la animación de muerte del enemigo
                    if (contact.getFixtureA().isSensor() && contact.getFixtureB().getUserData() == e && player.getCurrentlyAttacking()) {

                        System.out.println(enemies.indexOf(e));
                        if (e.body.getFixtureList().size > 0) {
                            e.killCharacter();
                            enemyCounter--;
                        }

                        if (enemyCounter == 0) {
                            dungeonTheme.stop();
                            victoryTheme.play();
                        }
                    }
                }
            }


            @Override
            public void endContact(Contact contact) {
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }

    public void handleTouchEvent(float delta) {
        afterScreenLapse += delta;
        if (!player.isAlive()) {
            if (Gdx.input.justTouched() && afterScreenLapse >= 14) {
                game.setScreen(new MainMenuScreen(game));
                dispose();
            }
        } else {
            if (Gdx.input.justTouched() && afterScreenLapse >= 7) {
                game.setScreen(new MainMenuScreen(game));
                dispose();
            }
        }
    }

    public void gameOverTextureUpdate(float dt) {
        if (opacity < 1)
            opacity += dt * 0.2;
        gameOverSprite.setAlpha(opacity);
        gameOverSprite.setColor(gameOverSprite.getColor().r, gameOverSprite.getColor().g, gameOverSprite.getColor().b, opacity);
        gameOverSprite.setBounds(camera.position.x - 200 / GameClass.PPM, camera.position.y - 100 / GameClass.PPM, texture.getWidth() / GameClass.PPM, texture.getHeight() / GameClass.PPM);
    }

    public void succesTextureUpdate(float dt) {
        if (opacity < 1)
            opacity += dt * 0.4;
        successSprite.setAlpha(opacity);
        successSprite.setColor(successSprite.getColor().r, successSprite.getColor().g, successSprite.getColor().b, opacity);
        successSprite.setBounds(camera.position.x - 200 / GameClass.PPM, camera.position.y - 100 / GameClass.PPM, texture.getWidth() / GameClass.PPM, texture.getHeight() / GameClass.PPM);
    }

    public GameController getController() {
        return controller;
    }

    public void instanceEnemies(TextureAtlas goblin, int goblinNumber) {

        ArrayList<Coordinate> coordinates = new ArrayList<Coordinate>();

        coordinates.add(new Coordinate(200,600));
        coordinates.add(new Coordinate(100,1100));
        coordinates.add(new Coordinate(900,1200));
        coordinates.add(new Coordinate(500,1000));
        coordinates.add(new Coordinate(700,100));
        coordinates.add(new Coordinate(1200,300));
        coordinates.add(new Coordinate(1300,700));
        coordinates.add(new Coordinate(600,700));
        coordinates.add(new Coordinate(100,270));

        Random randomEnemyPosition,randomVelocity;
        int randomPosition;
        enemies = new ArrayList<Enemy>();

        while (goblinNumber > 0) {
            randomEnemyPosition = new Random();
            randomPosition = randomEnemyPosition.nextInt(coordinates.size());
            Coordinate cords = coordinates.get(randomPosition);
            randomVelocity = new Random();

            enemy = new Enemy(world,this,goblin,cords.x,cords.y);
            enemy.body.setLinearVelocity((randomVelocity.nextInt((6-2) + 1) + 2) / 10f,0);
            enemy.body.getFixtureList().get(0).setUserData(enemy);
            enemies.add(enemy);

            coordinates.remove(randomPosition);
            goblinNumber--;
        }
    }

    public int getCurrentEnemyCounter() {
        return enemyCounter;
    }

    public void setMusicClips() {
        dungeonTheme = Gdx.audio.newMusic(Gdx.files.internal(("audio/music/dungeonTheme.mp3")));
        dungeonTheme.setVolume(0.2f);
        dungeonTheme.setLooping(true);

        gameOverTheme = Gdx.audio.newMusic(Gdx.files.internal(("audio/music/gameOverTheme.mp3")));
        gameOverTheme.setVolume(0.2f);

        victoryTheme = Gdx.audio.newMusic(Gdx.files.internal(("audio/music/victoryTheme.mp3")));
        victoryTheme.setVolume(0.5f);

        dungeonTheme.play();
    }

    public void moveAccordingLastDirection(float dt) {
        switch (lastButtonPressed) {
            case 1:
                player.body.setLinearVelocity(player.body.getPosition().x, player.body.getPosition().y * (-150 * dt));
                break;
            case 2:
                player.body.setLinearVelocity(player.body.getPosition().x * (-150 * dt), player.body.getPosition().y);
                break;
            case 3:
                player.body.setLinearVelocity(player.body.getPosition().x * (150 * dt), player.body.getPosition().y);
                break;

            case 4:
                player.body.setLinearVelocity(player.body.getPosition().x, player.body.getPosition().y * (150 * dt));
                break;
            default:
                player.body.setLinearVelocity(player.body.getPosition().x, player.body.getPosition().y);
        }
    }

    public class Coordinate {
        public int x,y;

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
