package jaumebalmes.dam.m06.com.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import jaumebalmes.dam.m06.com.GameClass;

public class DifficultyScreen implements Screen {

    private GameClass game;
    private Stage stage;
    private Texture texture;

    public DifficultyScreen (GameClass game, Texture texture){
        this.game = game;
        this.texture = texture;

        /// create stage and set it as input processor
        ScreenViewport screenViewport = new ScreenViewport();
        screenViewport.setUnitsPerPixel(0.5f);
        stage = new Stage(screenViewport);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();
        table.setFillParent(true);
        //table.setDebug(true);
        stage.addActor(table);

        // temporary until we have asset manager in
        Skin skin = new Skin(Gdx.files.internal("skin/golden-ui-skin.json"));

        //create buttons
        TextButton easy = new TextButton("Easy", skin);
        TextButton normal = new TextButton("Normal", skin);
        TextButton hard = new TextButton("Hard", skin);

        //add buttons to table
        table.add(easy).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(normal).fillX().uniformX();
        table.row();
        table.add(hard).fillX().uniformX();

        table.padTop(100);

        // create button listeners
        easy.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PlayScreen(game,9,5));
                dispose();
            }
        });

        normal.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PlayScreen(game,6,7));
                dispose();
            }
        });

        hard.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PlayScreen(game,3,9));
                dispose();
            }
        });

    }

    @Override
    public void render(float delta) {
        // clear the screen ready for next set of images to be drawn
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.sb.begin();
        game.sb.draw(texture,0,0);
        game.sb.end();

        // tell our stage to do actions and draw itself
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        // change the stage's viewport when teh screen size is changed
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // dispose of assets when not needed anymore
        stage.dispose();
    }


}
