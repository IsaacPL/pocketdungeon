package jaumebalmes.dam.m06.com.overlay;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import jaumebalmes.dam.m06.com.GameClass;

public class Hud implements Disposable {
    public Stage stage;
    private Viewport viewport;

    Label countdownLabel, countdownHP, timeLabel, enemiesCounterLabel, enemiesAliveLabel, hpLabel;

    public Hud(SpriteBatch sb) {

        viewport = new FitViewport(GameClass.V_WIDTH * 1.25f, GameClass.V_HEIGHT * 1.25f, new OrthographicCamera());
        stage = new Stage(viewport, sb);

        Table table = new Table();
        table.top();
        table.setFillParent(true);

        countdownLabel = new Label("UNDEFINED Time",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        countdownHP = new Label("UNDEFINED Hp",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeLabel = new Label("TIME",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        enemiesCounterLabel = new Label("UNDEFINED Inner",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        enemiesAliveLabel = new Label("ENEMIES ALIVE",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        hpLabel = new Label("HP",
                new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        table.add(hpLabel).expandX().padTop(10);
        table.add(enemiesAliveLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.row();
        table.add(countdownHP).expandX();
        table.add(enemiesCounterLabel).expandX();
        table.add(countdownLabel).expandX();

        stage.addActor(table);
    }

    public void updateLabels(int currentPlayerHP, int currentEnemiesAlive, float timer, int initialTime) {
        countdownHP.setText(currentPlayerHP);
        enemiesCounterLabel.setText(currentEnemiesAlive);
        countdownLabel.setText((int) timer);

        if (timer <= initialTime * 0.4)
            countdownLabel.setColor(Color.RED);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
